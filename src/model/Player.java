package model;

public class Player extends MobileUnit {
	
	public int experiencePoints;
	public int coins;
	
	public Player()
	{
		super.setHp(200);
		super.setAttackDamage(1);
		experiencePoints = 0;
		coins = 0;
	}
	
	public int getXp()
	{
		return experiencePoints;
	}
	
	public int getCoins()
	{
		return coins;
	}
	
	public void addXp(int xpGained)
	{
		experiencePoints += xpGained;
	}
	
	public void setMoney(int newCoins)
	{
		coins = newCoins;
	}
	
}
