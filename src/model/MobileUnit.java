package model;

public class MobileUnit {

	protected int hp;
	protected int attackDamage;
	protected boolean alive;
	protected int bounty; //How much money the player gets from killing it
	
	public int getHp()
	{
		return hp;
	}
	
	public void setHp(int newHp)
	{
		hp = newHp;
	}

	public int getAttackDamage()
	{
		return attackDamage;
	}
	
	public void setAttackDamage(int newDamage)
	{
		attackDamage = newDamage;
	}
	
	public int getBounty()
	{
		return bounty;
	}
	
	public void setBounty(int newBounty)
	{
		bounty = newBounty;
	}
	
	public void die()
	{
		alive = false;
	}
	
}
