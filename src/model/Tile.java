package model;
import java.awt.Image;

import javax.swing.ImageIcon;

/**
 * This class represents a graphical Tile in the game.
 * If you use it, you should EXTEND it or ADD MORE PROPERTIES
 */
public class Tile  {

	private ImageIcon icon;		// Graphical representation of this tile
	private String fileName;	// Filename should match the ImageIcon used
	private boolean breakable;
	
	public Tile() {
		this.icon = icon;
		this.fileName = fileName;
	}
	
	public ImageIcon getImageIcon() {
		return icon;
	}

	public Image getImage() {
		return icon.getImage();
	}
	
	public String getFileName() {
		return fileName;
	}

	public boolean getBreakable()
	{
		return breakable;
	}
	
	public void setBreakable(boolean newValue)
	{
		breakable = newValue;
	}
	
	
	@Override
	public String toString() {
		return "[" + fileName + "]";
	}
}