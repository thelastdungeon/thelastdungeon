package model;

import java.util.Random;

public class Model {
	
	private BackgroundTile[][] backgroundLayer;
	private MobileUnit[][] mobs; //All units that have hp or attack damage, monsters, the player
	private int numRows;
	private int numCols;
	
	public Model(int numRows, int numCols) {
		this.numRows = numRows;
		this.numCols = numCols;		
		backgroundLayer = new BackgroundTile[numRows][numCols];
		mobs = new MobileUnit[numRows][numCols];
	}
	
	public void setTileAt(int r, int c, BackgroundTile img) {
		backgroundLayer[r][c] = img;
	}
	

	//Use this to move players, monsters, and maybe projectiles in the future
	
	public void moveMobileUnit(int r, int c, MobileUnit mob) {
		mobs[r][c] = mob;
	}
	
	public BackgroundTile[][] getBackgroundLayer() {
		return backgroundLayer;
	}
	
	public MobileUnit[][] getMobileUnits() {
		return mobs;
	}
	
	public void setMobileUnits(MobileUnit[][] newMobs) {
		mobs = newMobs;
	}
	
	public void setGridSize(int numRows, int numCols) {

		// Temporary holding place for new data
		BackgroundTile newLayer[][] = new BackgroundTile[numRows][numCols];

		int newNumRows = Math.min(numRows, this.numRows);
		int newNumCols = Math.min(numCols, this.numCols);		

		// Copy old data into new array
		for (int r = 0; r < newNumRows; r++)
			for (int c = 0; c < newNumCols; c++)
				newLayer[r][c] = backgroundLayer[r][c];

		// Set new attributes
		backgroundLayer = newLayer;
		this.numRows = numRows;
		this.numCols = numCols;
	}
	
	private boolean isWithinBounds(int r, int c) {
		return r >= 0 && r < numRows() && c >= 0 && c < numCols();
	}	
	
	public int numRows() {
		return numRows;
	}	

	public int numCols() {
		return numCols;
	}
	
	
}
