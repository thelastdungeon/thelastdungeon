package model;

public class AttackUnit {
	
	
	private String attack = "";
	private boolean operation = false;
	private int row; 
	private int col;
	
	public AttackUnit(Boolean mode, String typeOfAttack) {
		operation = mode;
		attack = typeOfAttack;
		
	}
	
	public int getRow(){
		return row;
	}
	
	public void setRow(int a ){
		row = a;
	}
	
	public int getCol(){
		return col;
	}
	
	public void setCol(int a ){
		row = a;
	}
	
	public String getAttack(){
		return attack;
	}
	
	public boolean getOperation(){
		return operation;
	}
}
