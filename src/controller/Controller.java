package controller;

import java.awt.Canvas;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.Timer;

import model.AttackUnit;
import model.BackgroundTile;
import model.MobileUnit;
import model.Model;
import model.Monster_Blob;
import model.Player;
import view.View;

public class Controller {
	Image playerPic, enemyPic, groundPic, wallPic, fire1, fire2, fire3, fire4, fire5, boss_blobfishPic;

	Monster_Blob ENEMY;
	Monster_Blob BOSS;
	Player PLAYER;
	int[][] moves = { { -1, -1 }, { -1, 0 }, { -1, 1 }, { 0, -1 }, { 0, 1 }, { 1, 1 }, { 1, 0 }, { 1, -1 } };
	int rows = 12;
	int cols = 12;
	Model mod;
	MobileUnit[][] main;
	int playerR = 1;
	int playerC = 1;
	int enemyR = rows - 2;
	int enemyC = cols - 2;
	int attackR;
	int attackC;
	int fireDelay = 100;
	int numAttacks = 0;
	String direction = "down";
	int currentAttackCol;
	int currentAttackRow;
	boolean blobAlive = true;
	boolean bossAlive = false;
	
	int level = 1;
	
	Timer timer;
	Timer tim1 = new Timer(fireDelay, new myActionListener1());
	Timer tim2 = new Timer(fireDelay, new myActionListener2());
	Timer tim3 = new Timer(fireDelay, new myActionListener3());
	Timer tim4 = new Timer(fireDelay, new myActionListener4());
	BackgroundTile[][] back;
	BackgroundTile background;
	AttackUnit[][] attack;
	AttackUnit destruction;
	View view;

	public Controller() {
		mod = new Model(rows, cols);
		main = mod.getMobileUnits();

		back = mod.getBackgroundLayer();
		attack = new AttackUnit[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				background = new BackgroundTile(true);
				back[i][j] = background;
				destruction = new AttackUnit(false, "none");
				attack[i][j] = destruction;
			}
		}

		setLevel(level);

		view = new View(this);
		view.repaint();

		timer = new Timer(1000, new myActionListener());
		timer.start();
		back = mod.getBackgroundLayer();
		ENEMY = new Monster_Blob();
		BOSS = new Monster_Blob();
		BOSS.setHp(30);
		PLAYER = new Player();
		main[enemyR][enemyC] = ENEMY;
		main[playerR][playerC] = PLAYER;

		// Declare the images

		try {
			playerPic = ImageIO.read(new File("images/muscle_facing_down.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			enemyPic = ImageIO.read(new File("images/slime_front_1.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			groundPic = ImageIO.read(new File("images/city_ground1.JPG"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			wallPic = ImageIO.read(new File("images/wall.PNG"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			boss_blobfishPic = ImageIO.read(new File("images/boss_blobfish.JPG"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			fire1 = ImageIO.read(new File("images/muscle_areaAttack1.JPG"));
			fire2 = ImageIO.read(new File("images/muscle_areaAttack2.JPG"));
			fire3 = ImageIO.read(new File("images/muscle_areaAttack3.JPG"));
			fire4 = ImageIO.read(new File("images/muscle_areaAttack4.JPG"));
			fire5 = ImageIO.read(new File("images/muscle_areaAttack5.JPG"));

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		view.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				if (playerR == enemyR && playerC == enemyC) {
					main[enemyR][enemyC] = ENEMY;
				} else {
					main[playerR][playerC] = null;
				}
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					

					if(direction == "up"){
						playerR--;
						enemyCollision();
						if (playerR < 0 || !back[playerR][playerC].getPropertyWalkable()) {
							playerR++;

						}
						
					}else{
						direction = "up";
						setFacingImage(direction);

					}

				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					
					
					if(direction == "down"){
						playerR++;

						enemyCollision();
						if (playerR >= rows || !back[playerR][playerC].getPropertyWalkable()) {
							playerR--;
						}
						
					}else{
						direction = "down";
						setFacingImage(direction);

					}
					
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					
					
					if(direction == "left"){
						playerC--;
						enemyCollision();
						if (playerC < 0 || !back[playerR][playerC].getPropertyWalkable()) {
							playerC++;
						}
					}else{
						direction = "left";
						setFacingImage(direction);

					}
					
				} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					
					
					if(direction == "right"){
						playerC++;
						enemyCollision();
						if (playerC >= cols || !back[playerR][playerC].getPropertyWalkable()) {
							playerC--;
						}
					}else{
						direction = "right";
						setFacingImage(direction);

					}

				} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {

					if (anyTimerRunning()) {
						stopTimers();
					}
					//clearAttackBoard();
					attack();

				}

				main[playerR][playerC] = PLAYER;
			}

			private void stopTimers() {
				tim1.stop();
				tim2.stop();
				tim3.stop();
				tim4.stop();

			}

			private boolean anyTimerRunning() {
				return tim1.isRunning() || tim2.isRunning() || tim3.isRunning() || tim4.isRunning();
			}

			// ATTACK METHOD
			private void attack() {

				if (direction.equals("down")) {

					// facing down

					if (playerR + 1 < rows && main[playerR + 1][playerC] == ENEMY) {
						ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
						checkAlive(ENEMY, playerR+1, playerC);

					}
					// make fire image

					if (playerR + 1 < rows && back[playerR + 1][playerC].getPropertyWalkable()
							&& main[playerR + 1][playerC] != ENEMY) {
						attack[playerR + 1][playerC] = new AttackUnit(true, "fire");
						currentAttackCol = playerC;
						currentAttackRow = playerR + 1;
						attackR = playerR + 1;
						attackC = playerC;
						tim3.start();

					}

				} else if (direction.equals("up")) {

					// facing down

					if (playerR - 1 >= 0 && main[playerR - 1][playerC] == ENEMY) {
						ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
						checkAlive(ENEMY, playerR - 1, playerC);
						
					}
					// make fire image

					if (playerR - 1 >= 0 && back[playerR - 1][playerC].getPropertyWalkable()
							&& main[playerR - 1][playerC] != ENEMY) {
						attack[playerR - 1][playerC] = new AttackUnit(true, "fire");
						currentAttackCol = playerC;
						currentAttackRow = playerR - 1;
						attackR = playerR - 1;
						attackC = playerC;
						tim1.start();

					}

				} else if (direction.equals("left")) {

					// facing left

					if (playerC - 1 >= 0 && main[playerR][playerC - 1] == ENEMY) {
						ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
						checkAlive(ENEMY, playerR, playerC - 1);

					}
					// make fire image

					if (playerC - 1 >= 0 && back[playerR][playerC - 1].getPropertyWalkable()
							&& main[playerR][playerC - 1] != ENEMY) {

						attack[playerR][playerC - 1] = new AttackUnit(true, "fire");
						currentAttackCol = playerC-1;
						currentAttackRow = playerR;
						attackR = playerR;
						attackC = playerC - 1;
						tim2.start();

					}

				} else if (direction.equals("right")) {

					// facing right

					if (playerC + 1 < cols && main[playerR][playerC + 1] == ENEMY) {
						ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
						checkAlive(ENEMY, playerR, playerC + 1);

					}
					// make fire image

					if (playerC + 1 < cols && back[playerR][playerC + 1].getPropertyWalkable()
							&& main[playerR][playerC + 1] != ENEMY) {

						attack[playerR][playerC + 1] = new AttackUnit(true, "fire");
						currentAttackCol = playerC + 1;
						currentAttackRow = playerR;
						attackR = playerR;
						attackC = playerC + 1;
						tim4.start();

					}

				} else {

					System.out.println("This state should not be reached. Something might be broken");

				}

			}

			private void checkAlive(Monster_Blob monster, int row, int col) {
				// TODO Auto-generated method stub
				if(monster.getHp() <= 0){
					System.out.println("Monster has been defeated");
					main[row][col] = null;
					
					if(monster == ENEMY){
						main[1][1] = BOSS;
						bossAlive = true;
						
						blobAlive = false;
					}else{
						//moster is the boss
						bossAlive = false;
					}
					
					clearMonsterBoard();
				}
			}

			private void clearMonsterBoard() {
				// TODO Auto-generated method stub
				for (int i = 0; i < rows; i++) {
					for (int j = 0; j < cols; j++) {
						if(main[i][j] == ENEMY){
							main[i][j] = null;
						}
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

		});
	}

	private void setLevel(int level2) {
		// TODO Auto-generated method stub
		
		
		//Setting up initial background for level 1
		
		for (int i = 0; i < rows; i++) {
			/*
			Random r = new Random();
			int randomRow = r.nextInt(numRows());
			int randomCol = r.nextInt(numCols());
			background = new BackgroundTile(false);
			back[randomRow][randomCol] = background;
			*/
			
			background = new BackgroundTile(false);
			back[0][i] = background;
			back[i][0] = background;
			back[i][cols - 1] = background;
			back[rows - 1][i] = background;
		}
	}

	public void moveAgain(String direction) {
		attack[attackR][attackC] = new AttackUnit(false, "");
		if (direction.equals("down")) {

			// facing down
			
			if (attackR + 1 < rows && main[attackR + 1][attackC] == ENEMY) {
				ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
				System.out.println(ENEMY.getHp());
				if(ENEMY.getHp() <= 0)
				{
					PLAYER.setMoney(PLAYER.getCoins()+ENEMY.getBounty());
					System.out.println("You got " + ENEMY.getBounty() + " coins! You now have " + PLAYER.getCoins() + " coins!");
				}
					
				clearAttackBoard();
				tim3.stop();
			}
			// make fire image

			if (attackR + 1 < rows && back[attackR + 1][attackC].getPropertyWalkable() && 
					main[attackR + 1][attackC] != ENEMY) {

				attack[attackR + 1][attackC] = new AttackUnit(true, "fire");
				attackR++;

			} else {
				clearAttackBoard();
				tim3.stop();
			}

		} else if (direction.equals("up")) {

			// facing up

			if (attackR - 1 >= 0 && main[attackR - 1][attackC] == ENEMY) {
				ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
				System.out.println(ENEMY.getHp());
				if(ENEMY.getHp() <= 0)
				{
					PLAYER.setMoney(PLAYER.getCoins()+ENEMY.getBounty());
					System.out.println("You got " + ENEMY.getBounty() + " coins! You now have " + PLAYER.getCoins() + " coins!");
				}
				clearAttackBoard();
				tim1.stop();
			}
			// make fire image

			if (attackR - 1 >= 0 && back[attackR - 1][attackC].getPropertyWalkable() 
					&& main[attackR - 1][attackC] != ENEMY) {
				attack[attackR - 1][attackC] = new AttackUnit(true, "fire");
				attackR--;

			} else {
				clearAttackBoard();
				tim1.stop();
			}

		} else if (direction.equals("left")) {

			// facing left

			if (attackC - 1 >= 0 && main[attackR][attackC - 1] == ENEMY) {
				ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
				System.out.println(ENEMY.getHp());
				if(ENEMY.getHp() <= 0)
				{
					PLAYER.setMoney(PLAYER.getCoins()+ENEMY.getBounty());
					System.out.println("You got " + ENEMY.getBounty() + " coins! You now have " + PLAYER.getCoins() + " coins!");
				}
				clearAttackBoard();
				tim2.stop();
			}
			// make fire image

			if (attackC - 1 >= 0 && back[attackR][attackC - 1].getPropertyWalkable()
					&& main[attackR][attackC - 1] != ENEMY) {

				attack[attackR][attackC - 1] = new AttackUnit(true, "fire");
				attackC--;

			} else {
				clearAttackBoard();
				tim2.stop();
			}

		} else if (direction.equals("right")) {

			// facing right

			if (attackC + 1 < cols && main[attackR][attackC + 1] == ENEMY) {
				ENEMY.setHp(ENEMY.getHp() - PLAYER.getAttackDamage());
				System.out.println(ENEMY.getHp());
				if(ENEMY.getHp() <= 0)
				{
					PLAYER.setMoney(PLAYER.getCoins()+ENEMY.getBounty());
					System.out.println("You got " + ENEMY.getBounty() + " coins! You now have " + PLAYER.getCoins() + " coins!");
				}
				clearAttackBoard();
				tim4.stop();
			}
			// make fire image

			if (attackC + 1 < cols && back[attackR][attackC + 1].getPropertyWalkable()
					&& main[attackR][attackC + 1] != ENEMY) {
				attack[attackR][attackC + 1] = new AttackUnit(true, "fire");
				attackC++;

			} else {
				clearAttackBoard();
				tim4.stop();
			}

		}
	}

	private void clearAttackBoard() {
		// TODO Auto-generated method stub
		/*for (int i = 0; i < attack.length; i++) {
			for (int j = 0; j < attack[0].length; j++) {
				attack[i][j] = new AttackUnit(false, "");
			}
		}*/
	}

	private class myActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (playerR == enemyR && playerC == enemyC) {
				main[enemyR][enemyC] = PLAYER;
			} else {
				main[enemyR][enemyC] = null;
			}
			int[][] move = new int[moves.length][2];
			int a = 0;
			for (int i = 0; i < moves.length; i++) {
				if (enemyR + moves[i][0] >= 0 && enemyR + moves[i][0] < rows && enemyC + moves[i][1] >= 0
						&& enemyC + moves[i][1] < cols) {
					if (back[enemyR + moves[i][0]][enemyC + moves[i][1]].getPropertyWalkable()) {
						move[a] = moves[i];
						a++;
					}
				}
			}
			int random = (int) (Math.random() * a);

			main[enemyR + move[random][0]][enemyC + move[random][1]] = ENEMY;
			enemyR = enemyR + move[random][0];
			enemyC = enemyC + move[random][1];

		}

	}

	private class myActionListener1 implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			clearAttackBoard();
			moveAgain("up");			
		}

	}

	private class myActionListener2 implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			clearAttackBoard();
			moveAgain("left");
		}

	}

	private class myActionListener3 implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

			clearAttackBoard();

			moveAgain("down");
		}

	}

	private class myActionListener4 implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

			clearAttackBoard();
			moveAgain("right");
		}

	}

	public void enemyCollision() {
		if (playerR == enemyR && playerC == enemyC) {
			// main[playerR][playerC].setHp(main[playerR][playerC].getHp() -
			// main[enemyR][enemyC].getAttackDamage());
			PLAYER.setHp(PLAYER.getHp() - ENEMY.getAttackDamage());
		}
	}

	public void setFacingImage(String a) {
		if (a.equals("left")) {
			try {
				playerPic = ImageIO.read(new File("images/muscle_facing_left.png"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (a.equals("down")) {
			try {
				playerPic = ImageIO.read(new File("images/muscle_facing_down.png"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (a.equals("up")) {
			try {
				playerPic = ImageIO.read(new File("images/muscle_facing_up.png"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if (a.equals("right")) {
			try {
				playerPic = ImageIO.read(new File("images/muscle_facing_right.png"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public int numRows() {
		// TODO Auto-generated method stub
		return rows;
	}

	public int numCols() {
		// TODO Auto-generated method stub
		return cols;
	}

	public Image getBackgroundImageAt(int r, int c) {
		// TODO Auto-generated method stub
		if (main[r][c] == PLAYER) {
			return playerPic;
		} else if (main[r][c] == ENEMY && blobAlive) {
			return enemyPic;
		
		} else if (main[r][c] == BOSS && bossAlive) {
			return boss_blobfishPic;
			
		}else{

			if (attack[r][c].getOperation()) {
				// if else statement for other attack methods in the future
				if (attack[r][c].getAttack().equals("fire")) {
					return fire1;
				} else {
					return groundPic;
				}
			} else if (back[r][c].getPropertyWalkable()) {
				return groundPic;
			} else {
				return wallPic;
			}
		}
	}

}