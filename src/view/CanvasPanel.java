package view;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;

import controller.Controller;

public class CanvasPanel extends JPanel {
	
	
	private static final long serialVersionUID = -7643413096529405862L;
	private Controller controller;
	
	public CanvasPanel(Controller controller) {
		this.controller = controller;
	}	
	
	
	
	
	@Override
	protected void paintComponent(Graphics g) {
		
		//Need numRows() and numCols() method from controller
		//And other various methods
		super.setOpaque(false);
		super.paintComponent(g);
		super.repaint();
		drawBackground(g);
	}

	/** Draws the main background tiles */
	private void drawBackground(Graphics g) {
		
		int cellWidth = cellWidth();
		int cellHeight = cellHeight();
		
		for (int r = 0; r < controller.numRows(); r++) {
			for (int c = 0; c < controller.numCols(); c++) {
				Image img = controller.getBackgroundImageAt(r, c);
				if (img != null)
					g.drawImage(img, c*cellWidth, r*cellHeight, cellWidth, cellHeight, null);
			}
		}
		
	}
	
	/** Cell width in pixels */
	private int cellWidth() {
		return getWidth() / controller.numCols();
	}

	/** Cell height in pixels */
	private int cellHeight() {
		return getHeight() / controller.numRows();
	}
}