

package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.io.FilenameFilter;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import controller.Controller;

public class View extends JFrame {

	private static final long serialVersionUID = 6652774452907126316L;
	
	public static final int MIN_CELL_WIDTH = 5;		// Minimum pixel width of cells when zooming in

	public JComboBox<Integer[]> foregroundBrush;	// Left-click ImageIcon selection
	public JComboBox<Integer[]> backgroundBrush;	// Right-click ImageIcon selection
	public Integer[] intArray;						// Used to build the JComboBoxes for tile selection
	public File[] listOfFiles;						// List of image Files to load
	public ImageIcon[] images;						// List of ImageIcons to display in the JComboBoxes
	public JMenuItem newEditorMenuItem;				// MenuItem for opening an additional window
	public JMenuItem loadMenuItem;					// MenuItem for loading a file
	public JMenuItem saveMenuItem;					// MenuItem for saving a file
	public JMenuItem gridSizeMenuItem;				// MenuItem for changing the grid size
	public JMenuItem zoomInMenuItem;				// MenuItem for zooming in
	public JMenuItem zoomOutMenuItem;				// MenuItem for zooming out
	public JPopupMenu popUpMenu;					// Menu that pops up when Ctrl+clicking a cell
	public JRadioButton brushButton;				// Paint tool button
	public JRadioButton eraserButton;				// Eraser tool button
	public JRadioButton selectionButton;			// Select tool button
	public JRadioButton moveButton;					// Move tool button
	public JButton copyButton;						// Copy tool button
	public JButton pasteButton;						// Paste tool button
	public JCheckBox tilePropertyWalkable;			// Tile property: walkable

	private JPanel contentPane;						// A custom content pane used for this JPanel
	private CanvasPanel canvasPanel;				// Where the user draws the level
	private JPanel canvasParent;					// This is what the canvasPanel sits inside of
	private JScrollPane scrollPane;					// Scrollbars attached to canvasParent

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public View(Controller controller) {
				
		// Set up window and content pane properties
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Level Editor");
		setBounds(300, 100, 620, 670);
		setMinimumSize(new Dimension(620, 670));
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));


        
        // Where the level is painted
		canvasPanel = new CanvasPanel(controller);
		canvasPanel.setPreferredSize(new Dimension(16*30, 16*30));
		canvasPanel.setBorder(new LineBorder(Color.BLACK, 1));
		canvasPanel.repaint();
		
		canvasParent = new JPanel();
		canvasParent.setBackground(Color.DARK_GRAY);
		
		// Wrap the canvasPanel in a parent and then add scroll bars to the parent
		canvasParent.add(canvasPanel);
		

		
		// Add GUI components to our content pane
		contentPane.add(canvasParent, BorderLayout.CENTER);
		this.setContentPane(contentPane);
		this.setVisible(true);
	}

	/** The main drawing canvas where the level is drawn */
	public CanvasPanel getCanvas() {
		return canvasPanel;
	}
	
	public void addMyMouseListeners(MouseAdapter m) {
		canvasPanel.addMouseListener(m);
		canvasPanel.addMouseMotionListener(m);
		canvasParent.addMouseWheelListener(m);
	}
	
	public void addMyButtonListeners(ActionListener b) {
		brushButton.addActionListener(b);
		eraserButton.addActionListener(b);
		selectionButton.addActionListener(b);
		moveButton.addActionListener(b);
		copyButton.addActionListener(b);
		pasteButton.addActionListener(b);		
	}

	public void addMyMenuListeners(ActionListener m) {
		newEditorMenuItem.addActionListener(m);
		loadMenuItem.addActionListener(m);
		saveMenuItem.addActionListener(m);
		gridSizeMenuItem.addActionListener(m);
		zoomInMenuItem.addActionListener(m);
		zoomOutMenuItem.addActionListener(m);
	}
	
	public void addMyPopUpMenuListeners(ActionListener a) {
		tilePropertyWalkable.addActionListener(a);		
	}
	
    /** Returns an ImageIcon, or null if the path was invalid. */
    private static ImageIcon createImageIcon(String path) {
        if (path != null) {
            return new ImageIcon(path);
        } else {
            System.err.println("Couldn't find file: " + path);
                return null;
        }
    }	
}
